﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Strasbourg.Server
{
    [XmlRoot("donnees")]
    public class ParkingData
    {
        [XmlArray("TableDonneesParking")]
        [XmlArrayItem("PRK")]
        public List<Parking> Parkings { get; set; }

        [XmlAttribute("ts")]
        public DateTime LastUpdate { get; set; }

        public ParkingData()
        {
            this.Parkings = new List<Parking>();
        }
    }
}
