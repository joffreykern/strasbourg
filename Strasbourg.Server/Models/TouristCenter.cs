﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Strasbourg.Server
{
    public class TouristCenter : CsvModel
    {
        public TouristCenter(string[] data) : base(data)
        {
            this.SetCleanName();
        }

        public override void SetCleanName()
        {
            this.Name = this.Name.Replace("Office du tourisme", "");
            base.SetCleanName();
        }
    }
}
