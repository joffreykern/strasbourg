﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Strasbourg.Server
{
    public class MultimediaLibrary : CsvModel
    {
        public MultimediaLibrary(string[] data) : base(data)
        {
            this.SetCleanName();
        }

        public override void SetCleanName()
        {
            this.Name = this.Name.Replace("Médiathèque", "")
                            .Replace("Bibliothèque", "")
                            .Replace("Point lecture", "");
            base.SetCleanName();
        }
    }
}
