﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Strasbourg.Server
{
    [DataContract]
    public class Tweet : BaseModel
    {
        public Tweet()
        {
            this.User = new UserTweet();
        }

        [DataMember(Name = "text")]
        public string Text { get; set; }

        [DataMember(Name = "created_at")]
        public string CreatedAt { get; set; }

        [DataMember(Name = "user")]
        public UserTweet User { get; set; }

        public string Url
        {
            get
            {
                string url = "";
                if (this.Text.Contains("http://"))
                {
                    string[] tmp = this.Text.Split(' ');
                    foreach (string s in tmp)
                    {
                        if (s.StartsWith("http://"))
                        {
                            url = s;
                            break;
                        }
                    }
                }

                return url;
            }
        }

        public bool ContainsUrl { get { return !string.IsNullOrEmpty(this.Url); } }
    }

    [DataContract]
    public class UserTweet
    {
        [DataMember(Name = "profile_image_url")]
        public string Image { get; set; }

        [DataMember(Name = "name")]
        public string Name { get; set; }
    }
}
