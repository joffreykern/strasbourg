﻿using System.Device.Location;
using System.Xml.Serialization;

namespace Strasbourg.Server
{
    public class Parking : GeolocalizableModel
    {
        [XmlAttribute("Etat")]
        public string State { get; set; }

        [XmlAttribute("Ident")]
        public string Id { get; set; }

        [XmlAttribute("InfoUsager")]
        public string Info { get; set; }

        [XmlAttribute("Libre")]
        public string FreePlaces { get; set; }

        [XmlAttribute("Total")]
        public string Total { get; set; }

        private string _internalName;
        [XmlAttribute("Nom")]
        public string InternalName
        {
            get { return _internalName; }
            set { _internalName = value;
                base.Name = _internalName;
            }
        }

        private string _shortName;
        [XmlAttribute("NomCourt")]
        public string ShortName
        {
            get { return _shortName; }
            set
            {
                SetProperty(ref _shortName, value);
            }
        }

        public override string PushpinContent
        {
            get
            {
                return this.Name;
            }
        }

        public bool IsFull
        {
            get { return (this.Info == "COMPLET" || this.State == "3"); }
        }
        public bool IsClose
        {
            get { return (this.State == "2" || this.Info == "FERME"); }
        }
        public bool IsFullOrClose { get { return this.IsFull || this.IsClose; } }
    }
}
