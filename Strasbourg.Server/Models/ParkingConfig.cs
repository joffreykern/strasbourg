﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Strasbourg.Server
{
    [XmlRoot("configuration")]
    public class ParkingConfig
    {
        [XmlArray("TableDesParcsDeStationnement")]
        [XmlArrayItem("PRK")]
        public List<Parking> Parkings { get; set; }

        public ParkingConfig()
        {
            this.Parkings = new List<Parking>();
        }
    }
}
