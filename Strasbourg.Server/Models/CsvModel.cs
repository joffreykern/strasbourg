﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Strasbourg.Server
{
    public class CsvModel : GeolocalizableModel
    {
        public string Link { get; set; }

        public CsvModel(string[] data)
        {
            this.PushpinTemplate = PushpinTemplate.Default;
            this.Name = data[0].Split('-').Last().Trim();
            this.SetLocation(data[1]);
            this.Link = data[2];
        }

        public override string ToString()
        {
            return this.Name;
        }
    }
}
