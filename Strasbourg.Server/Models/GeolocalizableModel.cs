﻿using System;
using System.Collections.Generic;
using System.Device.Location;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Strasbourg.Server
{
    public class GeolocalizableModel : BaseModel
    {
        private string _name;
        public string Name
        {
            get { return _name; }
            set { SetProperty(ref _name, value); }
        }

        private double _latitude;
        public double Latitude
        {
            get { return _latitude; }
            set { SetProperty(ref _latitude, value); }
        }

        private double _longitude;
        public double Longitude
        {
            get { return _longitude; }
            set { SetProperty(ref _longitude, value); }
        }

        private GeoCoordinate _location;
        public GeoCoordinate Location
        {
            get { return _location ?? (_location = new GeoCoordinate(this.Latitude, this.Longitude)); }
            set { SetProperty(ref _location, value); }
        }

        public PushpinTemplate PushpinTemplate { get; set; }

        public string DistanceToStr
        {
            get
            {
                if (this.DistanceTo == null || this.DistanceTo == 0.0)
                    return "";

                string result = "";
                string suffix = "m";
                if (this.DistanceTo >= 1000)
                {
                    suffix = "km";
                    result = string.Format("{0:0.#}{1}", this.DistanceTo / 1000, suffix);
                }
                else
                {
                    result = string.Format("{0:0.#}{1}", this.DistanceTo, suffix);
                }

                return result;
            }
        }

        private double _distanceTo;
        public double DistanceTo
        {
            get { return _distanceTo; }
            set { SetProperty(ref _distanceTo, value); }
        }

        public void SetDistanceTo(GeoCoordinate coordinate)
        {
            if(coordinate.IsUnknown)
                return;

            this.DistanceTo = coordinate.GetDistanceTo(this.Location);
        }

        public virtual string PushpinContent { get; set; }

        public virtual void SetCleanName()
        {
            string[] stringSeparators = new string[] { " ", "-", "'" };
            string[] words = this.Name.Split(stringSeparators, StringSplitOptions.RemoveEmptyEntries);
            string nameString = "";

            foreach (string word in words)
            {
                nameString += char.ToUpper(word[0]) + word.Substring(1).ToLower() + " ";
            }

            nameString = nameString.Replace("L ", "L'");
            this.Name = nameString.Replace("D ", "D'");
        }

        public void SetLocation(string coordinates, bool lambertFormat = false)
        {
            string[] tabs = coordinates.Split(',');

            if (lambertFormat)
            {
                double[] tmp = LambertToGps.Convert(tabs[0].ParseToDouble(), tabs[1].ParseToDouble(),
                                                    LambertVersion.LambertI);
                this.Latitude = tmp[0];
                this.Longitude = tmp[1];
            }
            else
            {
                this.Latitude = tabs[0].ParseToDouble();
                this.Longitude = tabs[1].ParseToDouble();
            }
        }
    }

    public enum PushpinTemplate
    {
        Default,
        Autotrement,
        VelhopStation,
        VelhopShop,
        VelhopMixte
    }
}
