﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Strasbourg.Server.Models.Dailymotion
{
    class DailymotionRootObject
    {
        public int page { get; set; }
        public int limit { get; set; }
        public bool @explicit { get; set; }
        public bool has_more { get; set; }
        public List<DailymotionApiItem> list { get; set; }
    }
}
