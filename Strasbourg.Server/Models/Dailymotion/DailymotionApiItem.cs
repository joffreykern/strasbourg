﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Strasbourg.Server.Models.Dailymotion
{
    class DailymotionApiItem
    {
        public string id { get; set; }
        public string title { get; set; }
        public string channel { get; set; }
        public string owner { get; set; }
    }
}
