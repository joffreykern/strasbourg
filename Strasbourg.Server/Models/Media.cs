﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Strasbourg.Server
{
    public enum MEDIA_TYPE { VIDEO = 1, PHOTO }

    public class Media
    {
        public Media(string id, MEDIA_TYPE type)
        {
            this.Id = id;
            this.Type = type;

            switch (type)
            {
                case MEDIA_TYPE.VIDEO:
                    this.Url = string.Format("http://www.dailymotion.com/video/{0}", this.Id);
                    this.Image = string.Format("http://www.dailymotion.com/thumbnail/160x120/video/{0}", this.Id);
                    break;
            }
        }

        public string Id { get; private set; }
        public string Title { get; set; }
        public string Url { get; set; }
        public string Image { get; set; }
        public MEDIA_TYPE Type { get; set; }
    }
}
