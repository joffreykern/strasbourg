﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Strasbourg.Server.Models
{
    [DataContract]
    public class DailymotionApiItem
    {
        [DataMember(Name = "id")]
        public string Id { get; set; }
        [DataMember(Name = "title")]
        public string Title { get; set; }
        [DataMember(Name = "channel")]
        public string Channel { get; set; }
        [DataMember(Name = "owner")]
        public string Owner { get; set; }
    }

    [DataContract]
    public class DailymotionRootObject
    {
        [DataMember(Name = "page")]
        public int Page { get; set; }
        [DataMember(Name = "limit")]
        public int limit { get; set; }
        [DataMember(Name = "explicit")]
        public bool Explicit { get; set; }
        [DataMember(Name = "has_more")]
        public bool HasMore { get; set; }
        [DataMember(Name = "list")]
        public List<DailymotionApiItem> List { get; set; }
    }
}
