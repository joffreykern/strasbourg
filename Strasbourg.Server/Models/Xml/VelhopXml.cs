﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Strasbourg.Server.Models.Xml
{

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://ogr.maptools.org/")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://ogr.maptools.org/", IsNullable = false)]
    public partial class FeatureCollection
    {

        private boundedBy boundedByField;

        private featureMember[] featureMemberField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.opengis.net/gml")]
        public boundedBy boundedBy
        {
            get
            {
                return this.boundedByField;
            }
            set
            {
                this.boundedByField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("featureMember", Namespace = "http://www.opengis.net/gml")]
        public featureMember[] featureMember
        {
            get
            {
                return this.featureMemberField;
            }
            set
            {
                this.featureMemberField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.opengis.net/gml")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.opengis.net/gml", IsNullable = false)]
    public partial class boundedBy
    {

        private boundedByCoord[] boxField;

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("coord", IsNullable = false)]
        public boundedByCoord[] Box
        {
            get
            {
                return this.boxField;
            }
            set
            {
                this.boxField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.opengis.net/gml")]
    public partial class boundedByCoord
    {

        private decimal xField;

        private decimal yField;

        /// <remarks/>
        public decimal X
        {
            get
            {
                return this.xField;
            }
            set
            {
                this.xField = value;
            }
        }

        /// <remarks/>
        public decimal Y
        {
            get
            {
                return this.yField;
            }
            set
            {
                this.yField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.opengis.net/gml")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.opengis.net/gml", IsNullable = false)]
    public partial class featureMember
    {

        private poi_velhop poi_velhopField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://ogr.maptools.org/")]
        public poi_velhop poi_velhop
        {
            get
            {
                return this.poi_velhopField;
            }
            set
            {
                this.poi_velhopField = value;
            }
        }

        private poi_autotrement poi_autotrementField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://ogr.maptools.org/")]
        public poi_autotrement poi_autotrement
        {
            get
            {
                return this.poi_autotrementField;
            }
            set
            {
                this.poi_autotrementField = value;
            }
        }

    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://ogr.maptools.org/")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://ogr.maptools.org/", IsNullable = false)]
    public partial class poi_velhop
    {

        private poi_velhopGeometryProperty geometryPropertyField;

        private string libelleField;

        private string adresseField;

        private string typeField;

        private string statusField;

        private string fidField;

        /// <remarks/>
        public poi_velhopGeometryProperty geometryProperty
        {
            get
            {
                return this.geometryPropertyField;
            }
            set
            {
                this.geometryPropertyField = value;
            }
        }

        /// <remarks/>
        public string libelle
        {
            get
            {
                return this.libelleField;
            }
            set
            {
                this.libelleField = value;
            }
        }

        /// <remarks/>
        public string adresse
        {
            get
            {
                return this.adresseField;
            }
            set
            {
                this.adresseField = value;
            }
        }

        /// <remarks/>
        public string type
        {
            get
            {
                return this.typeField;
            }
            set
            {
                this.typeField = value;
            }
        }

        /// <remarks/>
        public string status
        {
            get
            {
                return this.statusField;
            }
            set
            {
                this.statusField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string fid
        {
            get
            {
                return this.fidField;
            }
            set
            {
                this.fidField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://ogr.maptools.org/")]
    public partial class poi_velhopGeometryProperty
    {

        private Point pointField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.opengis.net/gml")]
        public Point Point
        {
            get
            {
                return this.pointField;
            }
            set
            {
                this.pointField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.opengis.net/gml")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.opengis.net/gml", IsNullable = false)]
    public partial class Point
    {

        private string coordinatesField;

        /// <remarks/>
        public string coordinates
        {
            get
            {
                return this.coordinatesField;
            }
            set
            {
                this.coordinatesField = value;
            }
        }
    }


}
