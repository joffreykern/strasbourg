﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Strasbourg.Server.Models.Xml
{

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://ogr.maptools.org/")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://ogr.maptools.org/", IsNullable = false)]
    public partial class poi_autotrement
    {

        private poi_autotrementGeometryProperty geometryPropertyField;

        private object nb_placeField;

        private string libelleField;

        private string fidField;

        /// <remarks/>
        public poi_autotrementGeometryProperty geometryProperty
        {
            get
            {
                return this.geometryPropertyField;
            }
            set
            {
                this.geometryPropertyField = value;
            }
        }

        /// <remarks/>
        public string libelle
        {
            get
            {
                return this.libelleField;
            }
            set
            {
                this.libelleField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string fid
        {
            get
            {
                return this.fidField;
            }
            set
            {
                this.fidField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://ogr.maptools.org/")]
    public partial class poi_autotrementGeometryProperty
    {

        private Point pointField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.opengis.net/gml")]
        public Point Point
        {
            get
            {
                return this.pointField;
            }
            set
            {
                this.pointField = value;
            }
        }
    }


}
