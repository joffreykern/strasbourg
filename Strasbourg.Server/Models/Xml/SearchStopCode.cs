﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Strasbourg.Server.Models.Xml
{
    public class SearchStopCodesRequest
    {
        public static string Request = "<s:Envelope xmlns:s=\"http://schemas.xmlsoap.org/soap/envelope/\"><s:Header>"
              + "<h:CredentialHeader xmlns:h=\"http://www.cts-strasbourg.fr/\" xmlns=\"http://www.cts-strasbourg.fr/\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">"
              + "<ID>90</ID><MDP>bacplus5</MDP></h:CredentialHeader></s:Header>"
              + "<s:Body xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">"
              + "<rechercherCodesArretsDepuisLibelle xmlns=\"http://www.cts-strasbourg.fr/\"><Saisie>Sainte Hélène</Saisie><NoPage>1</NoPage></rechercherCodesArretsDepuisLibelle>"
              + "</s:Body></s:Envelope>";
    }


    #region RESPONSE

    public partial class EnvelopeBody
    {

        private rechercherCodesArretsDepuisLibelleResponse rechercherCodesArretsDepuisLibelleResponseField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.cts-strasbourg.fr/")]
        public rechercherCodesArretsDepuisLibelleResponse rechercherCodesArretsDepuisLibelleResponse
        {
            get
            {
                return this.rechercherCodesArretsDepuisLibelleResponseField;
            }
            set
            {
                this.rechercherCodesArretsDepuisLibelleResponseField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.cts-strasbourg.fr/")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.cts-strasbourg.fr/", IsNullable = false)]
    public partial class rechercherCodesArretsDepuisLibelleResponse
    {

        private rechercherCodesArretsDepuisLibelleResponseRechercherCodesArretsDepuisLibelleResult rechercherCodesArretsDepuisLibelleResultField;

        /// <remarks/>
        public rechercherCodesArretsDepuisLibelleResponseRechercherCodesArretsDepuisLibelleResult rechercherCodesArretsDepuisLibelleResult
        {
            get
            {
                return this.rechercherCodesArretsDepuisLibelleResultField;
            }
            set
            {
                this.rechercherCodesArretsDepuisLibelleResultField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.cts-strasbourg.fr/")]
    public partial class rechercherCodesArretsDepuisLibelleResponseRechercherCodesArretsDepuisLibelleResult
    {

        private bool suiteField;

        private rechercherCodesArretsDepuisLibelleResponseRechercherCodesArretsDepuisLibelleResultArret[] listeArretField;

        /// <remarks/>
        public bool Suite
        {
            get
            {
                return this.suiteField;
            }
            set
            {
                this.suiteField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("Arret", IsNullable = false)]
        public rechercherCodesArretsDepuisLibelleResponseRechercherCodesArretsDepuisLibelleResultArret[] ListeArret
        {
            get
            {
                return this.listeArretField;
            }
            set
            {
                this.listeArretField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.cts-strasbourg.fr/")]
    public partial class rechercherCodesArretsDepuisLibelleResponseRechercherCodesArretsDepuisLibelleResultArret
    {

        private string libelleField;

        private ushort codeField;

        /// <remarks/>
        public string Libelle
        {
            get
            {
                return this.libelleField;
            }
            set
            {
                this.libelleField = value;
            }
        }

        /// <remarks/>
        public ushort Code
        {
            get
            {
                return this.codeField;
            }
            set
            {
                this.codeField = value;
            }
        }
    }


    #endregion
}
