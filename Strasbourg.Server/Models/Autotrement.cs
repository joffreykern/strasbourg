﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Strasbourg.Server
{
    public class Autotrement : GeolocalizableModel
    {
        public override void SetCleanName()
        {
            this.Name = this.Name.Replace("Station Auto'trement ", "");
            base.SetCleanName();
            this.PushpinTemplate = PushpinTemplate.Autotrement;
        }
        public override string PushpinContent
        {
            get
            {
                return this.Name;
            }
            set
            {
                base.PushpinContent = value;
            }
        }

        public override string ToString()
        {
            return this.Name;
        }
    }
}
