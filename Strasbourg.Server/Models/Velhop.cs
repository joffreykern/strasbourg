﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Strasbourg.Server
{
    public class Velhop : GeolocalizableModel
    {
        public Velhop(string type)
        {
            switch (type)
            {
                case("station"):
                    this.PushpinTemplate = PushpinTemplate.VelhopStation;
                    break;
                case("boutique"):
                    this.PushpinTemplate = PushpinTemplate.VelhopShop;
                    break;
                case("mixte"):
                    this.PushpinTemplate = PushpinTemplate.VelhopMixte;
                    break;
            }
        }
        public string Id { get; set; }
        public string Address { get; set; }

        public override string PushpinContent
        {
            get
            {
                return this.Name;
            }
            set
            {
                base.PushpinContent = value;
            }
        }

        public override void SetCleanName()
        {
            this.Name = this.Name.Replace("Vélhop", "")
                            .Replace("Station/boutique", "")
                            .Replace("Station", "")
                            .Replace("Boutique", "")
                            .Replace("\"", "");
            base.SetCleanName();
        }
    }
}
