﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.Xml.Serialization;
using Strasbourg.Server.Models.Xml;
using System;
using Newtonsoft.Json;
using Strasbourg.Server.Models;
using FlickrNet;
using System.Diagnostics;
using LinqToTwitter;
using System.Windows.Threading;

namespace Strasbourg.Server
{
    public static class ServerProvider
    {
        #region PARKINGS
        private static string PARKINGSPOSITION = "http://carto.strasmap.eu/store/data/module/parking_position.xml";
        private static string PARKINGDATAS = "http://jadyn.strasbourg.eu/jadyn/dynn.xml";
        private static string PARKINGNAMES = "http://jadyn.strasbourg.eu/jadyn/config.xml";

        public static async Task<ParkingData> RetrieveParkings()
        {
            ParkingData data;
            using (StreamReader reader = new StreamReader(await GetResponseStream(PARKINGDATAS)))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(ParkingData));
                data = (ParkingData)serializer.Deserialize(reader);
            }

            using (StreamReader reader = new StreamReader(await GetResponseStream(PARKINGNAMES)))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(ParkingConfig));
                ParkingConfig config = (ParkingConfig)serializer.Deserialize(reader);

                foreach (Parking parking in data.Parkings)
                {
                    List<Parking> parkings = config.Parkings.Where(x => x.Id == parking.Id).ToList();
                    if (parkings.Count() > 0)
                    {
                        parking.Name = parkings.First().Name;
                        parking.ShortName = parkings.First().ShortName;
                        parking.SetCleanName();
                    }
                }
            }

            using (StreamReader reader = new StreamReader(await GetResponseStream(PARKINGSPOSITION)))
            {
                XDocument xml = XDocument.Parse(reader.ReadToEnd());
                foreach (XElement element in xml.Descendants())
                {
                    XAttribute attributeX = element.Attribute("x");
                    XAttribute attributeY = element.Attribute("y");
                    XAttribute attributeId = element.Attribute("id");
                    if (attributeX != null && attributeY != null && attributeId != null)
                    {
                        List<Parking> parkings = data.Parkings.Where(x => x.Id == attributeId.Value).ToList();
                        if (parkings.Count > 0)
                        {
                            double[] coordinates = LambertToGps.Convert(attributeX.Value.ParseToDouble(),
                                                                      attributeY.Value.ParseToDouble(),
                                                                      LambertVersion.LambertI);
                            parkings.First().Longitude = coordinates[1];
                            parkings.First().Latitude = coordinates[0];
                        }
                    }
                }
            }

            data.Parkings = data.Parkings.OrderBy(x => x.Name).ToList();
            return data;
        }
        #endregion

        #region VELHOPS
        private static string VELHOPDATAS = "http://carto.strasmap.eu/store/data/module/poi_velhop.xml";

        public static async Task<List<Velhop>> RetrieveVelhop()
        {
            FeatureCollection velhops = (FeatureCollection) await DeserializeFromUrl(VELHOPDATAS, typeof (FeatureCollection));
            List<Velhop> result = new List<Velhop>();
            foreach (poi_velhop poiVelhop in velhops.featureMember.Select(x => x.poi_velhop))
            {
                Velhop velhop = new Velhop(poiVelhop.type);
                velhop.Address = poiVelhop.adresse;
                velhop.Id = poiVelhop.fid;
                velhop.SetLocation(poiVelhop.geometryProperty.Point.coordinates, true);
                velhop.Name = poiVelhop.libelle;
                velhop.SetCleanName();

                result.Add(velhop);
            }

            return result;
        }
        #endregion

        #region AUTOTREMENTS
        public static string AUTOTREMENTDATAS = "http://carto.strasmap.eu/store/data/module/poi_autotrement.xml";

        public static async Task<List<Autotrement>> RetrieveAutotrements()
        {
            FeatureCollection collection = (FeatureCollection)await DeserializeFromUrl(AUTOTREMENTDATAS, typeof(FeatureCollection));
            List<Autotrement> result = new List<Autotrement>();
            foreach (poi_autotrement poiAutotrement in collection.featureMember.Select(x => x.poi_autotrement))
            {
                Autotrement autotrement = new Autotrement();
                autotrement.Name = poiAutotrement.libelle;
                autotrement.SetLocation(poiAutotrement.geometryProperty.Point.coordinates, true);
                autotrement.SetCleanName();

                result.Add(autotrement);
            }

            return result;
        }
        #endregion

        #region DISCOVER
        public static string THEATERDATA = "http://media.strasbourg.eu/alfresco/d/d/workspace/SpacesStore/dad0d62e-83d8-4d1e-b212-23ddafac97b1/20120123-StrasPlus-Acces_salles_spectacle.csv";
        public static string MULTIMEDIALIBRARYDATA = "http://media.strasbourg.eu/alfresco/d/d/workspace/SpacesStore/52d7c137-8c45-41e0-b549-8887b3f15367/20120123-StrasPlus-Acces_mediatheques.csv";
        public static string TOURISTCENTERDATA = "http://media.strasbourg.eu/alfresco/d/d/workspace/SpacesStore/365e5538-a429-4ea4-bc27-f0720ffc3b1f/20120123-StrasPlus-Acces_offices_tourisme.csv";
        public static string EQUIPMENTDATA = "http://media.strasbourg.eu/alfresco/d/d/workspace/SpacesStore/e6119f10-6ce9-4d97-9f94-e907db51b8ad/20120123-StrasPlus-Acces_equipements.csv";

        public static async Task<List<Theater>> RetrieveTheaters()
        {
            List<Theater> result = new List<Theater>();
            using (MemoryStream memoryStream = new MemoryStream())
            {
                Stream stream = await GetResponseStream(THEATERDATA);
                stream.CopyTo(memoryStream);
                string csv = ShitEncodingToUFT8(memoryStream.ToArray());

                string[] tabs = csv.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
                for (int i = 1; i < tabs.Count(); i++)
                {
                    Theater theater = new Theater(tabs[i].Split(';'));
                    result.Add(theater);
                }
            }

            return result;
        }

        public static async Task<List<MultimediaLibrary>> RetrieveMultimediaLibraries()
        {
            List<MultimediaLibrary> result = new List<MultimediaLibrary>();
            using (MemoryStream memoryStream = new MemoryStream())
            {
                Stream stream = await GetResponseStream(MULTIMEDIALIBRARYDATA);
                stream.CopyTo(memoryStream);
                string csv = ShitEncodingToUFT8(memoryStream.ToArray());

                string[] tabs = csv.Split(new string[] {"\r\n"}, StringSplitOptions.RemoveEmptyEntries);
                for (int i = 1; i < tabs.Count(); i++)
                {
                    MultimediaLibrary multimedia = new MultimediaLibrary(tabs[i].Split(';'));
                    result.Add(multimedia);
                }
            }
            return result;
        }

        public static async Task<List<TouristCenter>> RetrieveTouristCenters()
        {
            List<TouristCenter> result = new List<TouristCenter>();
            using (MemoryStream memoryStream = new MemoryStream())
            {
                Stream stream = await GetResponseStream(TOURISTCENTERDATA);
                stream.CopyTo(memoryStream);
                string csv = ShitEncodingToUFT8(memoryStream.ToArray());

                string[] tabs = csv.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
                for (int i = 1; i < tabs.Count(); i++)
                {
                    TouristCenter touristCenter = new TouristCenter(tabs[i].Split(';'));
                    result.Add(touristCenter);
                }
            }
            return result;
        }

        public static async Task<List<Equipment>> RetrieveEquipments()
        {
            List<Equipment> result = new List<Equipment>();
            using (MemoryStream memoryStream = new MemoryStream())
            {
                Stream stream = await GetResponseStream(EQUIPMENTDATA);
                stream.CopyTo(memoryStream);
                string csv = ShitEncodingToUFT8(memoryStream.ToArray());

                string[] tabs = csv.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
                for (int i = 1; i < tabs.Count(); i++)
                {
                    Equipment touristCenter = new Equipment(tabs[i].Split(';'));
                    result.Add(touristCenter);
                }
            }
            return result;
        }
        #endregion

        #region TWEETS
        private static string TWEETDATAS = "http://api.twitter.com/1/statuses/user_timeline/strasbourg.json";

        [Obsolete("", true)]
        public static async Task<List<Tweet>> RetrieveTweets(int count = 0)
        {
            List<Tweet> tweets = new List<Tweet>();
            using (StreamReader reader = new StreamReader(await GetResponseStream(TWEETDATAS)))
            {
                string json = reader.ReadToEnd();
                tweets = JsonConvert.DeserializeObject<List<Tweet>>(json);
            }

            if (count != 0 && count < tweets.Count)
                tweets = tweets.GetRange(0, count);

            return tweets;
        }

        private static string TWITTERCONSUMERKEY = "ZHSw7zGNISu2xNewaVxEjg";
        private static string TWITTERCONSUMERSECRET = "Nu7Q61oZm1BdAAUmLfweI8leApKD4gR9h6dPD6MYpQ";
        private static string TWITTEROAUTHTOKEN = "1519269523-ayLaWrTfOqvdau8HX90aGDGiD9bwhXUJfJaqpoW";
        private static string TWITTERACCESSTOKEN = "7dsLNkTgdbnYrn1gOvD3iUi0qmxiCVFa7Hoc0OS9U0";

        public static Task<List<Tweet>> RetrieveTweetsv2(int count = 0)
        {
            SingleUserAuthorizer singleUser = new SingleUserAuthorizer()
                {
                    Credentials = new InMemoryCredentials()
                        {
                            ConsumerKey = TWITTERCONSUMERKEY,
                            ConsumerSecret = TWITTERCONSUMERSECRET,
                            OAuthToken = TWITTEROAUTHTOKEN,
                            AccessToken = TWITTERACCESSTOKEN
                        }
                };

            TaskCompletionSource<List<Tweet>> tcs = new TaskCompletionSource<List<Tweet>>();
            TwitterContext context = new TwitterContext(singleUser);
            context.Status.Where(tweet => tweet.Type == StatusType.Home && tweet.ScreenName == "Strasbourg" && tweet.Count == count).MaterializedAsyncCallback(
                asyncResponse =>
                    {
                        if (asyncResponse.Status == TwitterErrorStatus.Success)
                        {
                            List<Tweet> tweets = new List<Tweet>();
                            foreach (Status statuse in asyncResponse.State)
                            {
                                Tweet tweet = new Tweet();
                                tweet.Text = statuse.Text;
                                tweet.CreatedAt = statuse.CreatedAt.ToShortDateString();
                                tweet.User = new UserTweet();
                                tweet.User.Image = statuse.User.ProfileImageUrl;
                                tweet.User.Name = statuse.User.Name;
                                tweets.Add(tweet);
                            }
                            tcs.TrySetResult(tweets);
                        }
                    });

            return tcs.Task;
        }
        #endregion

        #region MEDIAS
        public static string STASDAILYMOTIONACCOUNT = "https://api.dailymotion.com/user/VilledeStrasbourg/videos";
        private static string FLICKRKEY = "91c0d2732f897dda9ab1ef1c3d33b6bb";
        private static string FLICKRSECRET = "e1d2c0dc5cf3876b";
        private static string FLICKRSTRASBOURGGROUPID = "89119093@N00";

        public static async Task<List<Media>> RetrieveVideos()
        {
            DailymotionRootObject dailyResult = (DailymotionRootObject)await DeserializeJsonFromUrl< DailymotionRootObject>(STASDAILYMOTIONACCOUNT);
            List<Media> medias = new List<Media>();

            foreach (DailymotionApiItem dailyItem in dailyResult.List)
            {
                Media media = new Media(dailyItem.Id, MEDIA_TYPE.VIDEO);
                media.Title = dailyItem.Title;
                medias.Add(media);
            }
            
            return medias;
        }

        public static async Task<List<Media>> RetrievePhotos()
        {
            List<Media> medias = new List<Media>();

            foreach (Photo photo in await RetrieveFlickrPhotosAsync())
            {
                Media media = new Media(photo.PhotoId, MEDIA_TYPE.PHOTO);
                media.Title = photo.Title;
                media.Image = photo.MediumUrl;
                media.Url = photo.WebUrl;
                medias.Add(media);
            }
            return medias;
        }

        private static Task<PhotoCollection> RetrieveFlickrPhotosAsync()
        {
            TaskCompletionSource<PhotoCollection> tcs = new TaskCompletionSource<PhotoCollection>();
            Flickr flickr = new Flickr(FLICKRKEY, FLICKRSECRET);
            flickr.GroupsPoolsGetPhotosAsync(FLICKRSTRASBOURGGROUPID, 1, 20, result =>
                {
                    if (!result.HasError)
                        tcs.TrySetResult(result.Result);
                });

            return tcs.Task;
        }
            
        private static async Task<object> DeserializeJsonFromUrl<T>(string url)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            HttpWebResponse response = await request.GetResponseAsync();
            object result = null;
            using (StreamReader reader = new StreamReader(response.GetResponseStream()))
            {
                result = JsonConvert.DeserializeObject<T>(reader.ReadToEnd());  
            }

            return result;
        }
        #endregion

        #region CTS

        public static string SEARCHSTOPBYNAME = "http://opendata.cts-strasbourg.fr/webservice_v4/Service.asmx";
        public static async Task<bool> RetrieveSearchStop(string name)
        {
            byte[] data = new UTF8Encoding().GetBytes(SearchStopCodesRequest.Request);
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(SEARCHSTOPBYNAME);
            request.Method = "POST";
            request.ContentLength = data.Length;
            request.ContentType = "text/xml";
            using (Stream stream = await request.GetRequestStreamAsync())
            {
                stream.Write(data, 0, data.Length);
            }

            HttpWebResponse response = await request.GetResponseAsync();
            string result = "";
            using (StreamReader reader = new StreamReader(response.GetResponseStream()))
            {
                result = reader.ReadToEnd();
            }
            return true;
        }
        #endregion

        private static string ShitEncodingToUFT8(byte[] bytes)
        {
            StringBuilder builder = new StringBuilder();
            for (int i = 0; i < bytes.Length; i++)
            {
                builder.Append((char)bytes[i]);
            }

            return builder.ToString();
        }

        private static async Task<object> DeserializeFromUrl(string url, Type type)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            HttpWebResponse response = await request.GetResponseAsync();
            object result = null;
            using (StreamReader reader = new StreamReader(response.GetResponseStream()))
            {
                XmlSerializer serializer = new XmlSerializer(type);
                result = serializer.Deserialize(reader);
            }

            return result;
        }

        private static async Task<Stream> GetResponseStream(string url)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            HttpWebResponse response = await request.GetResponseAsync();
            return response.GetResponseStream();
        }
    }
}
