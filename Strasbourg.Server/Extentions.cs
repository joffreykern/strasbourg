﻿using System;
using System.Globalization;
using System.IO;
using System.Net;
using System.Threading.Tasks;

namespace Strasbourg.Server
{
    public static class Extensions
    {
        public static Task<HttpWebResponse> GetResponseAsync(this HttpWebRequest request)
        {
            TaskCompletionSource<HttpWebResponse> taskComplete = new TaskCompletionSource<HttpWebResponse>();
            request.BeginGetResponse(asyncResponse =>
            {
                try
                {
                    HttpWebRequest responseRequest = (HttpWebRequest)asyncResponse.AsyncState;
                    HttpWebResponse someResponse =
                       (HttpWebResponse)responseRequest.EndGetResponse(asyncResponse);
                    taskComplete.TrySetResult(someResponse);
                }
                catch (WebException webExc)
                {
                    HttpWebResponse failedResponse = (HttpWebResponse)webExc.Response;
                    taskComplete.TrySetResult(failedResponse);
                }
            }, request);
            return taskComplete.Task;
        }

        public static Task<Stream> GetRequestStreamAsync(this HttpWebRequest request)
        {
            TaskCompletionSource<Stream> tcs = new TaskCompletionSource<Stream>();
            request.BeginGetRequestStream(async =>
                {
                    try
                    {
                        HttpWebRequest someRequest = (HttpWebRequest) async.AsyncState;
                        Stream str = someRequest.EndGetRequestStream(async);
                        tcs.TrySetResult(str);
                    }
                    catch (WebException ex)
                    {
                        HttpWebResponse failedResponse = (HttpWebResponse)ex.Response;
                        tcs.TrySetResult(failedResponse.GetResponseStream());
                    }
                }, request);

            return tcs.Task;
        }

        public static double ParseToDouble(this string txt)
        {
            return double.Parse(txt, NumberStyles.Float, new CultureInfo("en-gb"));
        }

    }
}
