﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Strasbourg.Server;

namespace Strasbourg.ViewModels
{
    public class MultimediaLibraryViewModel : GeolocalizableViewModel
    {
        public MultimediaLibraryViewModel() : base(DatatypeProvider.MultimediaLibrary)
        {
            
        }
    }
}
