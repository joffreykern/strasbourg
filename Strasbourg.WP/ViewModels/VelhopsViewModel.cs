﻿using System.Collections.ObjectModel;
using Strasbourg.Server;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Strasbourg.ViewModels
{
    public class VelhopsViewModel : GeolocalizableViewModel
    {
        public VelhopsViewModel() : base(DatatypeProvider.Velhop)
        {
        }
    }
}
