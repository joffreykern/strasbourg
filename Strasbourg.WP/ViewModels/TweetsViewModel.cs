﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Phone.Tasks;
using Strasbourg.Server;

namespace Strasbourg.ViewModels
{
    public class TweetsViewModel : BaseViewModel
    {
        private ObservableCollection<Tweet> _tweets;
        public ObservableCollection<Tweet> Tweets
        {
            get { return _tweets; }
            set { SetProperty(ref _tweets, value); }
        }

        public TweetsViewModel()
        {
            Refresh();
        }

        private async void Refresh()
        {
            this.IsLoading = true;
            this.Tweets = new ObservableCollection<Tweet>(await ServerProvider.RetrieveTweetsv2());
            this.IsLoading = false;
        }

        public void NavigateToUrl(string url)
        {
            if (string.IsNullOrEmpty(url))
                return;

            WebBrowserTask task = new WebBrowserTask();
            task.Uri = new Uri(url);
            task.Show();
        }
    }
}
