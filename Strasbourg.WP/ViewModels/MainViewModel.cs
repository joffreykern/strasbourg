﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Phone.Tasks;
using Strasbourg.Server;

namespace Strasbourg.ViewModels
{
    public class MainViewModel : BaseViewModel
    {
        private ObservableCollection<Tweet> _tweets;
        public ObservableCollection<Tweet> Tweets
        {
            get { return _tweets; }
            set { SetProperty(ref _tweets, value); }
        }

        public MainViewModel()
        {
            Refresh();
        }

        private async void Refresh()
        {
            //this.Tweets = new ObservableCollection<Tweet>(await ServerProvider.RetrieveTweets(5));
            this.Tweets = new ObservableCollection<Tweet>(await ServerProvider.RetrieveTweetsv2(5));
        }

        public void NavigateToUrl(string url)
        {
            if (string.IsNullOrEmpty(url))
                return;

            WebBrowserTask task = new WebBrowserTask();
            task.Uri = new Uri(url);
            task.Show();
        }

    }
}
