﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Strasbourg.Server;
using System.Collections.ObjectModel;

namespace Strasbourg.ViewModels
{
    public class ViewModelLocator
    {
        private MainViewModel _main;
        public MainViewModel Main
        {
            get { return _main ?? (_main = new MainViewModel()); }
            set { _main = value; }
        }

        private static ParkingsViewModel _parkings;
        public ParkingsViewModel Parkings
        {
            get { return _parkings ?? (_parkings = new ParkingsViewModel()); }
            set { _parkings = value; }
        }

        private static VelhopsViewModel _velhopses;
        public VelhopsViewModel Velhopses
        {
            get { return _velhopses ?? (_velhopses = new VelhopsViewModel()); }
            set { _velhopses = value; }
        }

        private static AutotrementsViewModel _autotrements;
        public AutotrementsViewModel Autotrements
        {
            get { return _autotrements ?? (_autotrements = new AutotrementsViewModel()); }
            set { _autotrements = value; }
        }

        private static TheatersViewModel _theaters;
        public TheatersViewModel Theaters
        {
            get { return _theaters ?? (_theaters = new TheatersViewModel()); }
            set { _theaters = value; }
        }

        private static MultimediaLibraryViewModel _multimediaLibrary;
        public MultimediaLibraryViewModel MultimediaLibrary
        {
            get { return _multimediaLibrary ?? (_multimediaLibrary = new MultimediaLibraryViewModel()); }
            set { _multimediaLibrary = value; }
        }

        private static TouristCenterViewModel _touristCenter;
        public TouristCenterViewModel TouristCenter
        {
            get { return _touristCenter ?? (_touristCenter = new TouristCenterViewModel()); }
            set { _touristCenter = value; }
        }

        private static EquipmentsViewModel _equipments;
        public EquipmentsViewModel Equipments
        {
            get { return _equipments ?? (_equipments = new EquipmentsViewModel()); }
            set { _equipments = value; }
        }

        private static MediasViewModel _medias;
        public MediasViewModel Medias
        {
            get { return _medias ?? (_medias = new MediasViewModel()); }
            set { _medias = value; }
        }

        private static TweetsViewModel _tweets;
        public TweetsViewModel Tweets
        {
            get { return _tweets ?? (_tweets = new TweetsViewModel()); }
            set { _tweets = value; }
        }
        
        private static MapViewModel _map;
        public MapViewModel Map
        {
            get
            {
                if(_map == null)
                    throw new Exception("MapViewModel not set, considerate to use ViewModelLocator.CreateMapViewModel()");
                return _map;
            }
            set { _map = value; }
        }

        public static MapViewModel CreateMapViewModel(ObservableCollection<GeolocalizableModel> coordinates)
        {
            return _map = new MapViewModel(coordinates);
        }

        public static void DestroyAutotrementsViewModel()
        {
            _autotrements = null;
        }


        public static void DestroyEquipementsViewModel()
        {
            _equipments = null;
        }
        public static void DestroyMultimediaLibraryViewModel()
        {
            _multimediaLibrary = null;
        }
        public static void DestroyTheathersViewModel()
        {
            _theaters = null;
        }
        public static void DestroyTourismCenterViewModel()
        {
            _touristCenter = null;
        }
        public static void DestroyTweetsViewModel()
        {
            _tweets = null;
        }
        public static void DestroyVelhopViewModel()
        {
            _velhopses = null;
        }
        public static void DestroyParkingsViewModel()
        {
            _parkings = null;
        }
    }
}
