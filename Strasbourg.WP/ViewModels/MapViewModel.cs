﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Strasbourg.Server;

namespace Strasbourg.ViewModels
{
    public class MapViewModel : BaseViewModel
    {
        private ObservableCollection<GeolocalizableModel> _coordinates;
        public ObservableCollection<GeolocalizableModel> Coordinates
        {
            get { return _coordinates; }
            set { SetProperty(ref _coordinates, value); }
        }

        public MapViewModel(ObservableCollection<GeolocalizableModel> coordinates)
        {
            this.Coordinates = coordinates;
        }
    }
}
