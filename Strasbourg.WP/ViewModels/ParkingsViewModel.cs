﻿using System.Collections.ObjectModel;
using System.Device.Location;
using System.Diagnostics;
using Microsoft.Phone.Tasks;
using Strasbourg.Server;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Strasbourg.ViewModels
{
    public class ParkingsViewModel : GeolocalizableViewModel
    {
        private ObservableCollection<Parking> _parkings;
        public ObservableCollection<Parking> Parkings
        {
            get { return _parkings; }
            set 
            { 
                SetProperty(ref _parkings, value);
                base.SetItems(new List<GeolocalizableModel>(_parkings));
            }
        }

        private DateTime _lastUpdate;
        public DateTime LastUpdate
        {
            get { return _lastUpdate; }
            set { SetProperty(ref _lastUpdate, value); }
        }
        
        public ParkingsViewModel() : base (DatatypeProvider.Parkings, false)
        {
            RefreshData();
        }

        private async void RefreshData()
        {
            this.IsLoading = true;

            ParkingData parkings = await ServerProvider.RetrieveParkings();
            this.Parkings = new ObservableCollection<Parking>(parkings.Parkings);

            foreach (Parking parking in Parkings)
            {
                parking.SetDistanceTo(App.CurrentLocation.GetCurrentPosition());
            }
            this.Parkings = new ObservableCollection<Parking>(this.Parkings.OrderBy(x => x.DistanceTo));

            // Remove and add closedParking to have closed parking at the end of the list
            List<Parking> closedParkings = this.Parkings.Where(x => x.IsClose).ToList();
            foreach (Parking closedParking in closedParkings)
            {
                this.Parkings.Remove(closedParking);
            }
            foreach (Parking closedParking in closedParkings)
            {
                this.Parkings.Add(closedParking);
            }

            this.LastUpdate = parkings.LastUpdate;

            this.IsLoading = false;
        }
    }
}
