﻿using System.Collections.ObjectModel;
using Microsoft.Phone.Tasks;
using Strasbourg.Server;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Strasbourg.ViewModels
{
    public class MediasViewModel : BaseViewModel
    {
        private ObservableCollection<Media> _photos;
        public ObservableCollection<Media> Photos
        {
            get { return _photos; }
            set { SetProperty(ref _photos, value); }
        }

        private ObservableCollection<Media> _movies;
        public ObservableCollection<Media> Movies
        {
            get { return _movies; }
            set { SetProperty(ref _movies, value); }
        }


        public MediasViewModel()
        {
            Refresh();
        }

        private async void Refresh()
        {
            this.Photos = new ObservableCollection<Media>(await ServerProvider.RetrievePhotos());
            this.Movies = new ObservableCollection<Media>(await ServerProvider.RetrieveVideos());
        }

        public void NavigateToUrl(string url)
        {
            Microsoft.Phone.Tasks.WebBrowserTask task = new WebBrowserTask();
            task.Uri = new Uri(url);
            task.Show();
        }
    }
}
