﻿using Microsoft.Phone.Tasks;
using Strasbourg.Server;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Strasbourg.ViewModels
{
    public class GeolocalizableViewModel : BaseViewModel
    {
        public event EventHandler LoadingItemsCompleted;

        private ObservableCollection<GeolocalizableModel> _items;
        public ObservableCollection<GeolocalizableModel> Items
        {
            get { return _items; }
            set { SetProperty(ref _items, value); }
        }

        public GeolocalizableViewModel(DatatypeProvider datatype, bool retriveDatas = true)
        {
            if (retriveDatas)
                Refresh(datatype);
        }

        public void ShowRoute(GeolocalizableModel geolocalizable)
        {
            MapsDirectionsTask task = new MapsDirectionsTask();
            string name = string.IsNullOrEmpty(geolocalizable.PushpinContent)
                              ? geolocalizable.Name
                              : geolocalizable.PushpinContent;
            task.End = new LabeledMapLocation(name, geolocalizable.Location);
            task.Show();
        }

        private async void Refresh(DatatypeProvider datatype)
        {
            this.IsLoading = true;
            List<GeolocalizableModel> tmp = null;
            switch (datatype)
            {
                case DatatypeProvider.Autotrement:
                    tmp = new List<GeolocalizableModel>(await ServerProvider.RetrieveAutotrements());
                    break;
                case DatatypeProvider.Velhop:
                    tmp = new List<GeolocalizableModel>(await ServerProvider.RetrieveVelhop());
                    break;
                case DatatypeProvider.Parkings:
                    tmp = new List<GeolocalizableModel>((await ServerProvider.RetrieveParkings()).Parkings);
                    break;
                case DatatypeProvider.Equipments:
                    tmp = new List<GeolocalizableModel>(await ServerProvider.RetrieveEquipments());
                    break;
                case DatatypeProvider.Theaters:
                    tmp = new List<GeolocalizableModel>(await ServerProvider.RetrieveTheaters());
                    break;
                case DatatypeProvider.TouristCenter:
                    tmp = new List<GeolocalizableModel>(await ServerProvider.RetrieveTouristCenters());
                    break;
                case DatatypeProvider.MultimediaLibrary:
                    tmp = new List<GeolocalizableModel>(await ServerProvider.RetrieveMultimediaLibraries());
                    break;
                default:
                    throw new ArgumentOutOfRangeException("datatype");
            }

            SetItems(tmp);
            this.IsLoading = false;
        }

        public void SetItems(List<GeolocalizableModel> geolocalizable)
        {
            foreach (GeolocalizableModel geolocalizableModel in geolocalizable)
            {
                geolocalizableModel.SetDistanceTo(App.CurrentLocation.GetCurrentPosition());
            }
            this.Items = new ObservableCollection<GeolocalizableModel>(geolocalizable.OrderBy(x => x.DistanceTo));

            if (LoadingItemsCompleted != null)
                LoadingItemsCompleted(this, new EventArgs());
        }
    }

    public enum DatatypeProvider
    {
        Autotrement,
        Velhop,
        Parkings,
        Theaters,
        MultimediaLibrary,
        TouristCenter,
        Equipments
    }
}
