﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.ComponentModel;
using MyToolkit.Multimedia;
using Strasbourg.ViewModels;

namespace Strasbourg.Views
{
    public partial class Velhops : PhoneApplicationPage
    {
        public Velhops()
        {
            InitializeComponent();
        }

        protected override void OnBackKeyPress(CancelEventArgs e)
        {
            if (YouTube.CancelPlay()) // used to abort current youtube download
                e.Cancel = true;
            else
            {
            }
            base.OnBackKeyPress(e);
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            YouTube.CancelPlay(); // used to reenable page
            ViewModelLocator.DestroyVelhopViewModel();
            base.OnNavigatedTo(e);
        }

        private void VideoTap (object sender, System.Windows.Input.GestureEventArgs e)
        {
            // SHOW VIDEO
            // https://www.youtube.com/watch?v=jRsRFMe9g60
            YouTube.Play("jRsRFMe9g60", true, YouTubeQuality.Quality480P);
        }

    }
}