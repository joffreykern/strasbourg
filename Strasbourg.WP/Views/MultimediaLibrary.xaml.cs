﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Strasbourg.ViewModels;

namespace Strasbourg.Views
{
    public partial class MultimediaLibrary : PhoneApplicationPage
    {
        public MultimediaLibrary()
        {
            InitializeComponent();
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            ViewModelLocator.DestroyMultimediaLibraryViewModel();
            base.OnNavigatedFrom(e);
        }
    }
}