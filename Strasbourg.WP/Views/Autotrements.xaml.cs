﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Strasbourg.ViewModels;
using Microsoft.Phone.Tasks;
using System.ComponentModel;
using MyToolkit.Multimedia;

namespace Strasbourg.Views
{
    public partial class Autotrements : PhoneApplicationPage
    {
        public Autotrements()
        {
            InitializeComponent();
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            ViewModelLocator.DestroyAutotrementsViewModel();
            base.OnNavigatedFrom(e);
        }

        protected override void OnBackKeyPress(CancelEventArgs e)
        {
            if (YouTube.CancelPlay()) // used to abort current youtube download
                e.Cancel = true;
            else
            {
            }
            base.OnBackKeyPress(e);
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            YouTube.CancelPlay(); // used to reenable page

            base.OnNavigatedTo(e);
        }

        private void VideoTap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            // SHOW VIDEO
            // http://www.youtube.com/v/6FazPP0vDMc
            YouTube.Play("6FazPP0vDMc", true, YouTubeQuality.Quality480P);
        }
    }
}