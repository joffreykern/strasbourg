﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Maps.Controls;
using Microsoft.Phone.Shell;
using System.Diagnostics;
using Microsoft.Phone.Maps.Toolkit;
using Strasbourg.ViewModels;

namespace Strasbourg.Views
{
    public partial class Maps : PhoneApplicationPage
    {
        public Maps()
        {
            InitializeComponent();
            this.Loaded += OnLoaded;
        }

        private void OnLoaded(object sender, RoutedEventArgs routedEventArgs)
        {
            ObservableCollection<DependencyObject> children  = Microsoft.Phone.Maps.Toolkit.MapExtensions.GetChildren(map);
            MapItemsControl itemsControl = children.FirstOrDefault(x => x.GetType() == typeof(MapItemsControl)) as MapItemsControl;
            itemsControl.ItemsSource = (this.DataContext as MapViewModel).Coordinates;
        }
    }
}