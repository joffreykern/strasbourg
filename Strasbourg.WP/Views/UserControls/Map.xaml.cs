﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Maps.Toolkit;
using Microsoft.Phone.Shell;
using Microsoft.Phone.Tasks;
using Strasbourg.Server;
using Strasbourg.ViewModels;
using GestureEventArgs = System.Windows.Input.GestureEventArgs;

namespace Strasbourg.Views.UserControls
{
    public partial class Map : UserControl
    {
        public Map()
        {
            InitializeComponent();
            this.Loaded += Map_Loaded;
            this.map.Loaded += map_Loaded;
        }

        void map_Loaded(object sender, RoutedEventArgs e)
        {
            Microsoft.Phone.Maps.MapsSettings.ApplicationContext.ApplicationId = "808c6545-2df5-47c6-9ab8-f8457f8cf391";
            Microsoft.Phone.Maps.MapsSettings.ApplicationContext.AuthenticationToken = "HkQuf7cjQdQPD6_oMi_D-Q";
        }

        void Map_Loaded(object sender, RoutedEventArgs e)
        {
            (this.DataContext as GeolocalizableViewModel).LoadingItemsCompleted += OnLoadingItemsCompleted;
        }

        private void OnLoadingItemsCompleted(object sender, EventArgs eventArgs)
        {
            ObservableCollection<DependencyObject> children = Microsoft.Phone.Maps.Toolkit.MapExtensions.GetChildren(map);
            MapItemsControl itemsControl = children.FirstOrDefault(x => x.GetType() == typeof(MapItemsControl)) as MapItemsControl;
            foreach (GeolocalizableModel item in (this.DataContext as GeolocalizableViewModel).Items)
            {
                Pushpin pushpin = new Pushpin();
                switch (item.PushpinTemplate)
                {
                    case Server.PushpinTemplate.Autotrement:
                        pushpin.Template = this.Resources["autotrementPushpinTemplate"] as ControlTemplate;
                        break;
                    case Server.PushpinTemplate.VelhopStation:
                        pushpin.Template = this.Resources["velhopStationPushpinTemplate"] as ControlTemplate;
                        break;
                    case Server.PushpinTemplate.VelhopShop:
                        pushpin.Template = this.Resources["velhopShopPushpinTemplate"] as ControlTemplate;
                        break;
                    case Server.PushpinTemplate.VelhopMixte:
                        pushpin.Template = this.Resources["velhopMixtePushpinTemplate"] as ControlTemplate;
                        break;
                    default:
                        if (PushpinTemplate != null) pushpin.Template = PushpinTemplate;
                        break;
                }

                pushpin.Content = item.PushpinContent;
                pushpin.GeoCoordinate = item.Location;
                pushpin.Tap += delegate(object o, GestureEventArgs args)
                    {
                        Microsoft.Phone.Tasks.MapsDirectionsTask task = new MapsDirectionsTask();
                        task.End = new LabeledMapLocation(item.PushpinContent, item.Location);
                        task.Show();
                    };
                itemsControl.Items.Add(pushpin);
            }
            
        }

        public ControlTemplate PushpinTemplate
        {
            get { return (ControlTemplate)GetValue(PushpinTemplateProperty); }
            set { SetValue(PushpinTemplateProperty, value); }
        }

        // Using a DependencyProperty as the backing store for PushpinTemplate.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty PushpinTemplateProperty =
            DependencyProperty.Register("PushpinTemplate", typeof(ControlTemplate), typeof(Map), new PropertyMetadata(null));
    }
}
