﻿using System.Collections.ObjectModel;
using Strasbourg.Server;
using Strasbourg.ViewModels;
using System;
using System.Collections.Generic;

namespace Strasbourg
{
    public static class NavigationService
    {
        public static void NavigateToMap(ObservableCollection<GeolocalizableModel> coordinates)
        {
            ViewModelLocator.CreateMapViewModel(coordinates);
            App.RootFrame.Navigate(new Uri("/Views/Maps.xaml", UriKind.Relative));
        }
    }
}
