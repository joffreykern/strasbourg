﻿using System;
using System.Device.Location;
using System.Diagnostics;

namespace Strasbourg
{
    public class CurrentLocation : IDisposable
    {
        private readonly GeoCoordinateWatcher _watcher;
        private GeoCoordinate _currentCoordinate;

        public CurrentLocation()
        {
            _currentCoordinate = new GeoCoordinate();

            _watcher = new GeoCoordinateWatcher(GeoPositionAccuracy.High);
            _watcher.PositionChanged += WatcherOnPositionChanged;
            _watcher.Start(false);
        }

        public GeoCoordinate GetCurrentPosition()
        {
            return _currentCoordinate;
        }

        private void WatcherOnPositionChanged(object sender, GeoPositionChangedEventArgs<GeoCoordinate> geoPositionChangedEventArgs)
        {
            _currentCoordinate = _watcher.Position.Location;
        }

        public void Dispose()
        {
            _watcher.PositionChanged -= WatcherOnPositionChanged;
            _watcher.Stop();
            _watcher.Dispose();
        }
    }
}
