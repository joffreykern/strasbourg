﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Microsoft.Phone.Tasks;
using Strasbourg.Resources;
using GestureEventArgs = System.Windows.Input.GestureEventArgs;

namespace Strasbourg
{
    public partial class MainPage : PhoneApplicationPage
    {
        // Constructor
        public MainPage()
        {
            InitializeComponent();
            //TryCatch();
        }

        //private async void TryCatch()
        //{
        //    await Server.ServerProvider.RetrieveSearchStop("Sainte");
        //}

        #region MAIN MENU NAVIGATION
        //See http://stackoverflow.com/a/13846083
        private void SlideToPage(int item)
        {
            HomePanorama.SetValue(Panorama.SelectedItemProperty, (PanoramaItem)HomePanorama.Items[item]);
            Panorama temp = HomePanorama;

            LayoutRoot.Children.Remove(HomePanorama);
            LayoutRoot.Children.Add(temp);
            LayoutRoot.UpdateLayout();
        }

        private void MenuContempler(object sender, System.Windows.Input.GestureEventArgs e)
        {
            SlideToPage(1);
        }

        private void MenuDecouvrir(object sender, System.Windows.Input.GestureEventArgs e)
        {
            SlideToPage(2);
        }

        private void MenuSeDeplacer(object sender, System.Windows.Input.GestureEventArgs e)
        {
            SlideToPage(3);
        }

        private void MenuSinformer(object sender, System.Windows.Input.GestureEventArgs e)
        {
            SlideToPage(4);
        }


        private void MenuAPropos(object sender, System.Windows.Input.GestureEventArgs e)
        {
            this.NavigationService.Navigate(new Uri("/Views/About.xaml", UriKind.Relative));
        }
        #endregion

        #region "SE DEPLACER" NAVIGATION
        private void SeDeplacerEnTram_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            //NavigationService.Navigate(new Uri("/Views/Cts.xaml", UriKind.Relative));
        }


        private void SeDeplacerEnVelhop_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            NavigationService.Navigate(new Uri("/Views/Velhops.xaml", UriKind.Relative));
        }


        private void SeDeplacerEnAutotrement_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            NavigationService.Navigate(new Uri("/Views/Autotrements.xaml", UriKind.Relative));
        }


        private void SeDeplacerParking(object sender, System.Windows.Input.GestureEventArgs e)
        {
            NavigationService.Navigate(new Uri("/Views/Parkings.xaml", UriKind.Relative));
        }
        #endregion

        #region "DECOUVRIR" NAVIGATION
        private void DecouvrirSalleDeFete_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            NavigationService.Navigate(new Uri("/Views/Theaters.xaml", UriKind.Relative));
        }

        private void DecouvrirMediatheque_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            NavigationService.Navigate(new Uri("/Views/MultimediaLibrary.xaml", UriKind.Relative));
        }

        private void DecouvrirOfficeDeTourisme_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            NavigationService.Navigate(new Uri("/Views/TouristCenters.xaml", UriKind.Relative));
        }

        private void DecouvrirEquipement_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            NavigationService.Navigate(new Uri("/Views/Equipments.xaml", UriKind.Relative));
        }
        #endregion

        #region "S'INFORMER" NAVIGATION
        private void MoreTweet_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            NavigationService.Navigate(new Uri("/Views/Tweets.xaml", UriKind.Relative));
        }
        #endregion

        #region CONTEMPLER
        private void FirsImageTapped(object sender, GestureEventArgs e)
        {
            Microsoft.Phone.Tasks.WebBrowserTask task = new WebBrowserTask();
            task.Uri = new Uri("http://www.flickr.com/photos/59524761@N07/5885110631/in/pool-strasbourg");
            task.Show();
        }

        private void SecondImageTapped(object sender, GestureEventArgs e)
        {
            Microsoft.Phone.Tasks.WebBrowserTask task = new WebBrowserTask();
            task.Uri = new Uri("http://www.flickr.com/photos/59524761@N07/5885111865/in/pool-strasbourg");
            task.Show();
        }

        private void ThirdImageTapped(object sender, GestureEventArgs e)
        {
            Microsoft.Phone.Tasks.WebBrowserTask task = new WebBrowserTask();
            task.Uri = new Uri("http://www.flickr.com/photos/kepsyn/8461210653/in/pool-strasbourg/");
            task.Show();
        }


        private void FirstVideoTapped(object sender, GestureEventArgs e)
        {
            Microsoft.Phone.Tasks.WebBrowserTask task = new WebBrowserTask();
            task.Uri = new Uri("http://www.dailymotion.com/video/xeax90_musique-traditionnelle-alsacienne-f_news");
            task.Show();
        }

        private void SecondVideoTapped(object sender, GestureEventArgs e)
        {
            Microsoft.Phone.Tasks.WebBrowserTask task = new WebBrowserTask();
            task.Uri = new Uri("http://www.dailymotion.com/video/xxsytu_strasbourg-mon-amour-la-retrospective-2013_news");
            task.Show();
        }

        private void ThirdVideoTapped(object sender, GestureEventArgs e)
        {
            Microsoft.Phone.Tasks.WebBrowserTask task = new WebBrowserTask();
            task.Uri = new Uri("http://www.dailymotion.com/video/xxfiap_le-sujet-de-19h-a-strasbourg_news#.UTO1_zDTu2U");
            task.Show();
        }

        private void MoreMedias_Tap(object sender, GestureEventArgs e)
        {
            NavigationService.Navigate(new Uri("/Views/Medias.xaml", UriKind.Relative));
        }

        
        #endregion
    }
}