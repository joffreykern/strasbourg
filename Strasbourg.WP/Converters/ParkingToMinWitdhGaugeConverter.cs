﻿using Strasbourg.Server;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace Strasbourg.Converters
{
    public class ParkingToMinWitdhGaugeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            Parking parking = (Parking)value;

            if (parking.IsFullOrClose)
            {
                return 0;
            }

            double ratio = (double.Parse(parking.FreePlaces) / double.Parse(parking.Total));
            return (int) 400 * ratio;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
