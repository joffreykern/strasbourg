﻿using Strasbourg.Server;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Media;

namespace Strasbourg.Converters
{
    public class VelhopTypeToColorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            PushpinTemplate type = (PushpinTemplate)value;

            SolidColorBrush color = new SolidColorBrush();

            switch (type)
            {
                case PushpinTemplate.VelhopMixte:
                    color.Color = Color.FromArgb(255, 245, 202, 43);
                    break;
                case PushpinTemplate.VelhopShop:
                    color.Color = Color.FromArgb(255, 153, 190, 38);
                    break;
                case PushpinTemplate.VelhopStation:
                    color.Color = Color.FromArgb(255, 0, 145, 200);
                    break;
            }

            return color;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
