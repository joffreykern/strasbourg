﻿using Strasbourg.Server;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace Strasbourg.Converters
{
    public class VelhopTypeToStringConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            PushpinTemplate type = (PushpinTemplate)value;
            string label = "INCONNU";

            switch (type)
            {
                case PushpinTemplate.VelhopMixte:
                    label = "STATION & BOUTIQUE";
                    break;
                case PushpinTemplate.VelhopShop:
                    label = "BOUTIQUE";
                    break;
                case PushpinTemplate.VelhopStation:
                    label = "STATION";
                    break;
            }

            return label;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
